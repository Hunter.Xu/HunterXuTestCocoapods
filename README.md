# HunterXuCocoapods

[![CI Status](http://img.shields.io/travis/HunterXu/HunterXuCocoapods.svg?style=flat)](https://travis-ci.org/HunterXu/HunterXuCocoapods)
[![Version](https://img.shields.io/cocoapods/v/HunterXuCocoapods.svg?style=flat)](http://cocoapods.org/pods/HunterXuCocoapods)
[![License](https://img.shields.io/cocoapods/l/HunterXuCocoapods.svg?style=flat)](http://cocoapods.org/pods/HunterXuCocoapods)
[![Platform](https://img.shields.io/cocoapods/p/HunterXuCocoapods.svg?style=flat)](http://cocoapods.org/pods/HunterXuCocoapods)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HunterXuCocoapods is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "HunterXuCocoapods"
```

## Author

HunterXu, zhanwei.xu@mfashion.com.cn

## License

HunterXuCocoapods is available under the MIT license. See the LICENSE file for more info.
